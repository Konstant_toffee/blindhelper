package com.example.blindhelper.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

public class AddFriendFragment extends Fragment {
    EditText searchUser;
    Button searchBtn, addFriendBtn, returnBtn;
    TextView searchShow;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.addfriend_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        searchUser = view.findViewById(R.id.search_user);
        searchBtn = view.findViewById(R.id.search_btn);
        addFriendBtn = view.findViewById(R.id.add_friend_btn);
        returnBtn = view.findViewById(R.id.return_to_mainpage);
        searchShow = view.findViewById(R.id.display_user);

        addFriendBtn.setVisibility(View.GONE);

        searchBtn.setOnClickListener(v -> {
            searchForUser();
        });

        addFriendBtn.setOnClickListener(z ->{
            addFriendInList();
        });

        returnBtn.setOnClickListener(x -> {
            navController.navigate(R.id.action_addFriend2_to_mainPage);
        });
    }

    private void searchForUser(){
        Document queryFilter  = new Document("username", searchUser.getText().toString().trim());

        model.getUsers().getValue().findOne(queryFilter).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                if(result == null){
                    String failure = "This user doesn't exist!";
                    searchShow.setText(failure);
                    addFriendBtn.setVisibility(View.GONE);
                } else if(result.getString("username").equals(model.getUserValue().getUsername())){
                    String failure = "You cannot add yourself in friend list!";
                    searchShow.setText(failure);
                    addFriendBtn.setVisibility(View.GONE);
                    //result.getList("FriendList", String.class).contains(model.getUserValue().getUsername());
                } else if(model.getUserValue().getFriendList().contains(result.getString("username"))){
                    String failure = "This user is already in your friend list!";
                    searchShow.setText(failure);
                    addFriendBtn.setVisibility(View.GONE);
                } else {
                    Log.v("EXAMPLE", "successfully found a document: " + result);
                    String data = result.get("username") + "\n" + result.get("firstName") + "\n" + result.get("lastName");
                    searchShow.setText(data);
                    addFriendBtn.setVisibility(View.VISIBLE);
                }
            } else {
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });
    }

    private void addFriendInList(){
        Document newRequest = new Document("sentRequest", model.getUserValue().getUsername()).append("receivedRequest", searchUser.getText().toString().trim());

        model.getFriendRequests().getValue().insertOne(newRequest).getAsync(task -> {
            if (task.isSuccess()) {
                Log.v("EXAMPLE", "successfully inserted a document with id: " + task.get());
            } else {
                Log.e("EXAMPLE", "failed to insert documents with: " + task.getError().getErrorMessage());
            }
        });

        String success = "Sent friend request to the user!";
        searchShow.setText(success);
        addFriendBtn.setVisibility(View.GONE);
    }
}
