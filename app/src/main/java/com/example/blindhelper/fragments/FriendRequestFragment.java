package com.example.blindhelper.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;
import com.example.blindhelper.model.DBFriendRequest;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

public class FriendRequestFragment extends Fragment {
    ListView friendRequests;
    Button returnBtn;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friendrequest_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        friendRequests = view.findViewById(R.id.friendRequest);
        returnBtn = view.findViewById(R.id.return_to_mainpage);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getActivity(), R.layout.items_of_friendrequest, R.id.senderOfRequest,  model.getReceivedFriendRequests()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_of_friendrequest, parent, false);
                }

                TextView senderOfRequest = convertView.findViewById(R.id.senderOfRequest);
                Button acceptFriendRequest = convertView.findViewById(R.id.acceptFriendRequest);
                Button denyFriendRequest = convertView.findViewById(R.id.denyFriendRequest);

                senderOfRequest.setText(model.getReceivedFriendRequests().get(position));

                acceptFriendRequest.setOnClickListener(x -> {
                    DBFriendRequest acceptRequest = new DBFriendRequest();
                    acceptRequest.setSentRequest(model.getReceivedFriendRequests().get(position));
                    acceptRequest.setReceivedRequest(model.getUserValue().getUsername());

                    handleAcceptRequest(acceptRequest);
                    deleteFriendRequest(model.getReceivedFriendRequests().get(position));
                    model.getReceivedFriendRequests().remove(position);
                    navController.navigate(R.id.action_friendRequest2_self);
                });

                denyFriendRequest.setOnClickListener(y -> {
                    deleteFriendRequest(model.getReceivedFriendRequests().get(position));
                    model.getReceivedFriendRequests().remove(position);
                    navController.navigate(R.id.action_friendRequest2_self);
                });
                return convertView;
            }
        };
        friendRequests.setAdapter(myAdapter);


        returnBtn.setOnClickListener(w -> {
            navController.navigate(R.id.action_friendRequest2_to_mainPage);
        });
    }

    private void deleteFriendRequest(String filter){
        model.getFriendRequests().getValue().deleteOne(new Document("sentRequest", filter).append("receivedRequest", model.getUserValue().getUsername())).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getDeletedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully deleted a document.");
                } else {
                    Log.v("EXAMPLE", "did not delete a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to delete document with: ", task.getError());
            }
        });
    }

    private void handleAcceptRequest(DBFriendRequest req){
        model.getUsers().getValue().updateOne(new Document("username", req.getReceivedRequest()), new Document("$push", new Document("FriendList", req.getSentRequest()))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }
        });

        model.getUsers().getValue().updateOne(new Document("username", req.getSentRequest()), new Document("$push", new Document("FriendList", req.getReceivedRequest()))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }
        });

    }
}
