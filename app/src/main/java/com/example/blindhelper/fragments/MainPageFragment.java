package com.example.blindhelper.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.activities.DetectorActivity;
import com.example.blindhelper.activities.MainClassActivity;
import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.realm.mongodb.RealmResultTask;
import io.realm.mongodb.mongo.iterable.MongoCursor;

import static android.app.Activity.RESULT_OK;



public class MainPageFragment extends Fragment {
    Button logoutBtn, callBtn, addFriendBtn, friendListBtn, friendRequestBtn, cameraBtn, sendCoordinatesBtn, mapBtn, speakBtn, receivedCoordinatesBtn;
    TextView display;
    ViewModelApp model;
    NavController navController;
    public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mainpage_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        display = view.findViewById(R.id.display);
        logoutBtn = view.findViewById(R.id.button_second);
        callBtn = view.findViewById(R.id.buttonCall);
        cameraBtn =  view.findViewById(R.id.camera);
        addFriendBtn = view.findViewById(R.id.addFriend);
        friendListBtn = view.findViewById(R.id.friendList);
        friendRequestBtn = view.findViewById(R.id.friendRequests);
        sendCoordinatesBtn = view.findViewById(R.id.findMe);
        receivedCoordinatesBtn = view.findViewById(R.id.helpPeople);
        mapBtn = view.findViewById(R.id.mapRedirect);
        speakBtn = view.findViewById(R.id.VoiceRec);

        if(model.getUserValue().isBlind()){
            callBtn.setVisibility(View.VISIBLE);
            cameraBtn.setVisibility(View.VISIBLE);
            sendCoordinatesBtn.setVisibility(View.VISIBLE);
            receivedCoordinatesBtn.setVisibility(View.GONE);
            speakBtn.setVisibility(View.VISIBLE);
        } else {
            callBtn.setVisibility(View.GONE);
            cameraBtn.setVisibility(View.GONE);
            sendCoordinatesBtn.setVisibility(View.GONE);
            receivedCoordinatesBtn.setVisibility(View.VISIBLE);
            speakBtn.setVisibility(View.GONE);
        }

        readData();
        callBtn.setOnClickListener(arg0 -> callEmergencyContact());
        sendCoordinatesBtn.setOnClickListener(arg1 -> findClosestUser());
        receivedCoordinatesBtn.setOnClickListener(arg2 -> receiveCoordinatesFromBlindUser());
        cameraBtn.setOnClickListener(arg3 -> openCamera());
        addFriendBtn.setOnClickListener(arg4 -> navController.navigate(R.id.action_mainPage_to_addFriend2));
        friendListBtn.setOnClickListener(arg5 -> openFriendList());
        friendRequestBtn.setOnClickListener(arg6 -> openFriendRequests());
        mapBtn.setOnClickListener(arg7 -> navController.navigate(R.id.action_mainPage_to_googleMapsFragment));
        speakBtn.setOnClickListener(arg8 -> startVoiceRecognitionActivity());
        logoutBtn.setOnClickListener(arg9 -> Logout());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void callEmergencyContact(){
        checkPermission();
        if(model.getUserValue().getCloseFriend().equals("")){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setTitle("No emergency contact");
            builder1.setMessage("You have to add an emergency contact in friend list first!");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok", (dialog, which) -> dialog.cancel());
            builder1.create().show();
        } else {
            model.getUsers().getValue().findOne(new Document("username", model.getUserValue().getCloseFriend())).getAsync(task -> {
                if (task.isSuccess()) {
                    Document result = task.get();
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + result.get("phone")));
                    startActivity(callIntent);
                } else {
                    Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                }
            });
        }
    }

    public void findClosestUser(){
        if(model.getUserValue().isFoundByFriendsOnly() && model.getUserValue().getFriendList().size() == 0){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setTitle("No users in friend list");
            builder1.setMessage("You cannot be found by friends because you don't have any! " +
                    "Add friends or turn off the option to be found by friends only!");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok", (dialog, which) -> dialog.cancel());
            builder1.create().show();
        } else {
            RealmResultTask<MongoCursor<Document>> findTask = model.getUserCoordinates().getValue().find().iterator();
            findTask.getAsync(task -> {
                        if (task.isSuccess()) {
                            MongoCursor<Document> results = task.get();
                            double lowestDistance = 0.0;
                            String closestUser = "";
                            if (results.hasNext()) {
                                while (results.hasNext()) {
                                    Document doc = results.next();
                                    if (!doc.getString("username").equals(model.getUserValue().getUsername())) {
                                        if (model.getUserValue().isFoundByFriendsOnly() && !model.getUserValue().getFriendList().contains(doc.getString("username"))) {
                                            continue;
                                        }
                                        double distance = Math.sqrt((model.getCoordinates().getValue().getLat() - doc.getDouble("lat")) * (model.getCoordinates().getValue().getLat() - doc.getDouble("lat"))
                                                + (model.getCoordinates().getValue().getLang() - doc.getDouble("lon")) * (model.getCoordinates().getValue().getLang() - doc.getDouble("lon")));
                                        if (lowestDistance == 0.0 || lowestDistance > distance) {
                                            closestUser = doc.getString("username");
                                            lowestDistance = distance;
                                        }
                                    }
                                }
                                model.setClosestUser(closestUser, model.getCoordinates().getValue().getLat(), model.getCoordinates().getValue().getLang());
                                navController.navigate(R.id.action_mainPage_to_sentCoordinates2);
                            }
                            Log.v("EXAMPLE", "successfully found a document: " + results);

                        } else {
                            Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                        }
                    }
            );
        }
    }

    public void receiveCoordinatesFromBlindUser(){
        model.getSendToHelper().getValue().findOne(new Document("helper", model.getUserValue().getUsername())).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                if(result == null){
                    model.setNeedingHelpUser("", 0.0, 0.0);
                } else {
                    model.setNeedingHelpUser(result.getString("needsHelp"), result.getDouble("lat"), result.getDouble("lon"));
                }
                navController.navigate(R.id.action_mainPage_to_receivedCoordinates);
            } else {
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void openCamera(){
        checkPermission();
        Intent localIntent = new Intent(getActivity(), DetectorActivity.class);
        startActivity(localIntent);
    }

    public void openFriendList(){
        Document queryFilter  = new Document("username", model.getUserValue().getUsername());
        model.getUsers().getValue().findOne(queryFilter).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                model.getUserValue().setFriendList((ArrayList<String>) result.getList("FriendList", String.class));
                navController.navigate(R.id.action_mainPage_to_friendList2);

            } else {
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });
    }

    public void openFriendRequests(){
        Document queryFilter  = new Document("receivedRequest", model.getUserValue().getUsername());
        RealmResultTask<MongoCursor<Document>> findTask = model.getFriendRequests().getValue().find(queryFilter).iterator();
        findTask.getAsync(task -> {
                    if (task.isSuccess()) {
                        MongoCursor<Document> results = task.get();
                        Log.v("EXAMPLE", "successfully found a document: " + results);
                        if (model.getReceivedFriendRequests().size() > 0){
                            model.getReceivedFriendRequests().clear();
                        }
                        if(results.hasNext()) {
                            while (results.hasNext()) {
                                String doc = results.next().getString("sentRequest");
                                model.getReceivedFriendRequests().add(doc);
                            }
                        }
                        navController.navigate(R.id.action_mainPage_to_friendRequest2);
                    } else {
                        Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                    }
                }
        );
    }

    public void Logout(){
        model.setUser(null);
        model.setCoordinates(null);
        model.setClosestUser(null, null, null);
        model.setNeedingHelpUser(null, null, null);
        if(model.getReceivedFriendRequests().size() > 0){
            model.getReceivedFriendRequests().clear();
        }
        model.setHelpAccepted(false);
        navController.navigate(R.id.action_mainPage_to_register);
    }

    private void readData(){
        String data = "Username: " + model.getUser().getValue().getUsername() + "\n"
                +"First Name: " + model.getUser().getValue().getFirstName() + "\n"
                +"Last Name: " + model.getUser().getValue().getLastName() + "\n"
                + "Email: " + model.getUser().getValue().getEmail() + "\n"
                + "Phone number: " + model.getUser().getValue().getPhone() + "\n";
        display.setText(data);
    }

    public void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speech recognition demo");
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches.contains("information") || matches.contains("Google information")) {
                String msg = "Commands for voice recognition: call, map, add friend, friend list, camera, friend request, i need help";
                MainClassActivity act = (MainClassActivity) getActivity();
                act.speak(msg);
            }
            else if(matches.contains("call") || matches.contains("Google call")){
                callEmergencyContact();
            }
            else if (matches.contains("map") || matches.contains("Google map") || matches.contains("Google Map")) {
                navController.navigate(R.id.action_mainPage_to_googleMapsFragment);
            }
            else if (matches.contains("add friend") || matches.contains("Google add friend")) {
                navController.navigate(R.id.action_mainPage_to_addFriend2);
            }
            else if (matches.contains("friend list") || matches.contains("Google friend list")) {
                openFriendList();
            }
            else if (matches.contains("camera") || matches.contains("Google camera")) {
                openCamera();
            }
            else if (matches.contains("friend request") || matches.contains("Google friend request")) {
                openFriendRequests();
            }
            else if (matches.contains("I need help") || matches.contains("Google I need help")) {
                findClosestUser();
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void checkPermission(){
        if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.CALL_PHONE,}, 1);
        }
        if(getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, 2);
        }
    }

}