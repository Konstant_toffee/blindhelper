package com.example.blindhelper.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

public class ReceivedCoordinatesFragment extends Fragment {

    Button acceptBtn, denyBtn, returnBtn;
    TextView searchShow;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.receivedcoordinates_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        acceptBtn = view.findViewById(R.id.accept_help);
        denyBtn = view.findViewById(R.id.deny_help);
        returnBtn = view.findViewById(R.id.return_to_mainpage);
        searchShow = view.findViewById(R.id.display_user);

        acceptBtn.setVisibility(View.GONE);
        denyBtn.setVisibility(View.GONE);

        if(model.getNeedingHelpUser().equals("")){
            String text = "No one needs your help right now.";
            searchShow.setText(text);
        } else {
            model.getUsers().getValue().findOne(new Document("username", model.getNeedingHelpUser())).getAsync(task -> {
                if (task.isSuccess()) {
                    Document result = task.get();
                    String data = result.get("username") + "\n" + result.get("firstName") + "\n" + result.get("lastName");
                    searchShow.setText(data);
                    acceptBtn.setVisibility(View.VISIBLE);
                    denyBtn.setVisibility(View.VISIBLE);
                } else {
                    Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                }
            });

            acceptBtn.setOnClickListener(x -> {
                model.setHelpAccepted(true);
                navController.navigate(R.id.action_receivedCoordinates_to_googleMapsFragment);
            });

            denyBtn.setOnClickListener(x -> {
                model.getSendToHelper().getValue().deleteOne(new Document("helper", model.getUserValue().getUsername())).getAsync(task -> {
                    if (task.isSuccess()) {
                        long count = task.get().getDeletedCount();
                        if (count == 1) {
                            Log.v("EXAMPLE", "successfully deleted a document.");
                        } else {
                            Log.v("EXAMPLE", "did not delete a document.");
                        }
                    } else {
                        Log.e("EXAMPLE", "failed to delete document with: ", task.getError());
                    }
                });
                navController.navigate(R.id.action_receivedCoordinates_to_mainPage);
            });
        }

        returnBtn.setOnClickListener(x -> navController.navigate(R.id.action_receivedCoordinates_to_mainPage));
    }
}