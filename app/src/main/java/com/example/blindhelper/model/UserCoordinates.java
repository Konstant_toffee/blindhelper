package com.example.blindhelper.model;

public class UserCoordinates {
    String username;
    Double lat;
    Double lang;

    public UserCoordinates() {
    }

    public UserCoordinates(String username, Double lat, Double lang) {
        this.username = username;
        this.lat = lat;
        this.lang = lang;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLang() {
        return lang;
    }

    public void setLang(Double lang) {
        this.lang = lang;
    }
}
