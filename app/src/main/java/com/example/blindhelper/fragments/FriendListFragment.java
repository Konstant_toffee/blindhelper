package com.example.blindhelper.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

public class FriendListFragment extends Fragment {
    ListView friendList;
    Button returnBtn;
    CheckBox foundFriendsOnly;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friendlist_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        friendList = view.findViewById(R.id.friendList);
        returnBtn = view.findViewById(R.id.return_to_mainpage);
        foundFriendsOnly = view.findViewById(R.id.foundFriendsOnly);

        if(!model.getUserValue().isBlind()){
            foundFriendsOnly.setVisibility(View.GONE);
        }

        foundFriendsOnly.setChecked(model.getUserValue().isFoundByFriendsOnly());


        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getActivity(), R.layout.items_of_friendlist, R.id.textView, model.getUserValue().getFriendList()){
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_of_friendlist, parent, false);
                }

                TextView textView = convertView.findViewById(R.id.textView);
                Button removeFriend = convertView.findViewById(R.id.removeFriend);
                Button makeBestFriend = convertView.findViewById(R.id.makeBestFriend);
                Button callFriend = convertView.findViewById(R.id.callFriend);

                textView.setText(model.getUserValue().getFriendList().get(position));

                if(model.getUserValue().getCloseFriend().equals(model.getUserValue().getFriendList().get(position)) || !model.getUserValue().isBlind()){
                    makeBestFriend.setVisibility(View.GONE);
                } else {
                    makeBestFriend.setVisibility(View.VISIBLE);
                }

                removeFriend.setOnClickListener(x -> {
                    RemoveFriend(model.getUserValue().getFriendList().get(position));
                    navController.navigate(R.id.action_friendList2_self);
                    //RemoveFriend(model.getUserValue().getFriendList().get(position), model.getUserValue().getUsername());
                });

                makeBestFriend.setOnClickListener(y -> {
                    MakeBestFriend(model.getUserValue().getFriendList().get(position));
                    navController.navigate(R.id.action_friendList2_self);
                    makeBestFriend.setVisibility(View.GONE);
                });

                callFriend.setOnClickListener(z -> {
                    CallFriend(model.getUserValue().getFriendList().get(position));
                });

                return convertView;
            }
        };

        friendList.setAdapter(myAdapter);

        foundFriendsOnly.setOnClickListener(v -> {
            if(model.getUserValue().getFriendList().size() == 0){
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setTitle("No users in friend list");
                builder1.setMessage("You must add a friend first to use that function!");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Ok", (dialog, which) -> dialog.cancel());
                builder1.create().show();
                foundFriendsOnly.setChecked(false);
            } else{
                boolean checked;
                checked = ((CheckBox) v).isChecked();
                IsFoundByFriends(checked);
            }
        });

        returnBtn.setOnClickListener(w -> {
            navController.navigate(R.id.action_friendList2_to_mainPage);
        });
    }

    public void RemoveFriend(String removedFriend){
        model.getUserValue().getFriendList().remove(removedFriend);
        model.getUsers().getValue().updateOne(new Document("username", model.getUserValue().getUsername()), new Document("$pull", new Document("FriendList", removedFriend))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }
        });

        model.getUsers().getValue().updateOne(new Document("username", removedFriend), new Document("$pull", new Document("FriendList", model.getUserValue().getUsername()))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }
        });

        if(model.getUserValue().getCloseFriend().equals(removedFriend)){
            model.getUserValue().setCloseFriend("");

            model.getUsers().getValue().updateOne(new Document("username", model.getUserValue().getUsername()), new Document("$set", new Document("closeFriend", ""))).getAsync(task -> {
                if (task.isSuccess()) {
                    long count = task.get().getModifiedCount();
                    if (count == 1) {
                        Log.v("EXAMPLE", "successfully updated a document.");
                    } else {
                        Log.v("EXAMPLE", "did not update a document.");
                    }
                } else {
                    Log.e("EXAMPLE", "failed to update document with: ", task.getError());
                }

            });
        }
    }

    public void MakeBestFriend(String newBestFriend){
        model.getUserValue().setCloseFriend(newBestFriend);

        model.getUsers().getValue().updateOne(new Document("username", model.getUserValue().getUsername()), new Document("$set", new Document("closeFriend", newBestFriend))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }

        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void CallFriend(String friend){
        Document queryFilterLocal  = new Document("username", friend);
        model.getUsers().getValue().findOne(queryFilterLocal).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                checkPermission();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + Uri.parse(result.getString("phone"))));
                startActivity(callIntent);
            } else {
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });
    }

    public void IsFoundByFriends(boolean checked){
        model.getUsers().getValue().updateOne(new Document("username", model.getUserValue().getUsername()), new Document("$set", new Document("foundByFriendsOnly", checked))).getAsync(task -> {
            if (task.isSuccess()) {
                long count = task.get().getModifiedCount();
                if (count == 1) {
                    Log.v("EXAMPLE", "successfully updated a document.");
                } else {
                    Log.v("EXAMPLE", "did not update a document.");
                }
            } else {
                Log.e("EXAMPLE", "failed to update document with: ", task.getError());
            }
        });
        model.getUserValue().setFoundByFriendsOnly(checked);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void checkPermission(){
        if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.CALL_PHONE,}, 1);
        }
    }

}
