package com.example.blindhelper.mathfunctions;

import java.util.Random;

public class MathFunctions {
    public static Double giveCoordinate(boolean isLat){
        double min;
        double max;

        if(isLat){
            min = -90;
            max = 90;
        } else {
            min = -180;
            max = 180;
        }

        Random r = new Random();
        return min + r.nextFloat() * (max - min);
    }
}
