package com.example.blindhelper.model;

public class DBFriendRequest {

    private String sentRequest;
    private String receivedRequest;

    public DBFriendRequest() {
    }

    public DBFriendRequest(String sentRequest, String receivedRequest) {
        this.sentRequest = sentRequest;
        this.receivedRequest = receivedRequest;
    }

    public String getSentRequest() {
        return sentRequest;
    }

    public void setSentRequest(String sentRequest) {
        this.sentRequest = sentRequest;
    }

    public String getReceivedRequest() {
        return receivedRequest;
    }

    public void setReceivedRequest(String receivedRequest) {
        this.receivedRequest = receivedRequest;
    }
}
