package com.example.blindhelper.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import io.realm.mongodb.mongo.options.UpdateOptions;

public class SentCoordinatesFragment extends Fragment {

    Button cancelBtn, returnBtn;
    TextView searchShow;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sentcoordinates_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);
        cancelBtn = view.findViewById(R.id.cancel_user);
        returnBtn = view.findViewById(R.id.return_to_mainpage);
        searchShow = view.findViewById(R.id.display_user);
        cancelBtn.setVisibility(View.GONE);

        model.getUsers().getValue().findOne(new Document("username", model.getClosestUser())).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                String data = result.get("username") + "\n" + result.get("firstName") + "\n" + result.get("lastName");
                searchShow.setText(data);
                cancelBtn.setVisibility(View.VISIBLE);
            } else {
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });

        Document sendHelper = new Document("helper", model.getClosestUser());
        sendHelper.append("needsHelp", model.getUserValue().getUsername());
        double lat = model.getCoordinates().getValue().getLat();
        double lon = model.getCoordinates().getValue().getLang();
        sendHelper.append("lat", lat);
        sendHelper.append("lon", lon);

        UpdateOptions options = new UpdateOptions();
        model.getSendToHelper().getValue().updateOne(new Document("helper", model.getClosestUser()), sendHelper, options.upsert(true)).getAsync(task -> {
            if (task.isSuccess()) {
                Log.v("EXAMPLE", "successfully inserted a document with id: " + task.get());
            } else {
                Log.e("EXAMPLE", "failed to insert documents with: " + task.getError().getErrorMessage());
            }
        });

        cancelBtn.setOnClickListener(x -> {
            model.getSendToHelper().getValue().deleteOne(new Document("helper", model.getClosestUser()).append("needsHelp", model.getUserValue().getUsername())).getAsync(task -> {
                if (task.isSuccess()) {
                    long count = task.get().getDeletedCount();
                    if (count == 1) {
                        Log.v("EXAMPLE", "successfully deleted a document.");
                    } else {
                        Log.v("EXAMPLE", "did not delete a document.");
                    }
                } else {
                    Log.e("EXAMPLE", "failed to delete document with: ", task.getError());
                }
            });
            navController.navigate(R.id.action_sentCoordinates2_to_mainPage);
        });

        returnBtn.setOnClickListener(x -> navController.navigate(R.id.action_sentCoordinates2_to_mainPage));
    }
}
