package com.example.blindhelper.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.blindhelper.model.LocalUser;
import com.example.blindhelper.model.UserCoordinates;

import org.bson.Document;

import java.util.ArrayList;

import io.realm.mongodb.App;
import io.realm.mongodb.AppConfiguration;
import io.realm.mongodb.Credentials;
import io.realm.mongodb.User;
import io.realm.mongodb.mongo.MongoClient;
import io.realm.mongodb.mongo.MongoCollection;
import io.realm.mongodb.mongo.MongoDatabase;

public class ViewModelApp extends ViewModel {
    private final MutableLiveData<LocalUser> user = new MutableLiveData<>();
    private final MutableLiveData<UserCoordinates> coordinates = new MutableLiveData<>();
    private final MutableLiveData<App> app = new MutableLiveData<>();
    private final MutableLiveData<User> mongoUser = new MutableLiveData<>();
    private final MutableLiveData<MongoClient> mongoClient = new MutableLiveData<>();
    private final MutableLiveData<MongoDatabase> mongoDatabase = new MutableLiveData<>();
    private final MutableLiveData<MongoCollection<Document>> users = new MutableLiveData<>();
    private final MutableLiveData<MongoCollection<Document>> friendRequests = new MutableLiveData<>();
    private final MutableLiveData<MongoCollection<Document>> userCoordinates = new MutableLiveData<>();
    private final MutableLiveData<MongoCollection<Document>> sendToHelper = new MutableLiveData<>();
    private String closestUser;
    private Double lat;
    private Double lon;
    private ArrayList<String> receivedFriendRequests = new ArrayList<>();
    private String needingHelpUser;
    private Double latHelp;
    private Double lonHelp;
    private boolean isHelpAccepted = false;

    public LiveData<LocalUser> getUser() {
        return user;
    }

    public void setUser(LocalUser currUser){
        user.setValue(currUser);
    }

    public LocalUser getUserValue(){
        return user.getValue();
    }

    public MutableLiveData<UserCoordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(UserCoordinates coordinates) {
        this.coordinates.setValue(coordinates);
    }

    public void setMongo(){
        setMongoClient();
        setMongoDatabase();
        setUsers();
        setUserCoordinates();
        setFriendRequests();
        setSendToHelper();
    }

    public void setApp() {
        this.app.setValue(new App(new AppConfiguration.Builder("blindhelper-qfcot").build()));
        Credentials credentials = Credentials.anonymous();
        app.getValue().loginAsync(credentials, result -> {
            if (result.isSuccess()) {
                Log.v("QUICKSTART", "Successfully authenticated anonymously.");
                User user = app.getValue().currentUser();
                System.out.println("Created anonymous user!");
                setMongoUser();
            } else {
                Log.e("QUICKSTART", "Failed to log in. Error: " + result.getError());
            }
        });
    }

    public void setMongoUser() {
        if(app.getValue().currentUser() == null){
            Credentials credentials = Credentials.anonymous();
            app.getValue().loginAsync(credentials, result -> {
                if (result.isSuccess()) {
                    Log.v("QUICKSTART", "Successfully authenticated anonymously.");
                    this.mongoUser.setValue( app.getValue().currentUser());
                    setMongo();
                } else {
                    Log.e("QUICKSTART", "Failed to log in. Error: " + result.getError());
                }
            });
        } else{
            this.mongoUser.setValue(app.getValue().currentUser());
            setMongo();
        }
    }

    public void setMongoClient() {
        this.mongoClient.setValue(mongoUser.getValue().getMongoClient("mongodb-atlas"));
    }

    public void setMongoDatabase() {
        this.mongoDatabase.setValue(mongoClient.getValue().getDatabase("allUsersAndOtherThings"));
    }

    public void setUsers() {
        this.users.setValue(mongoDatabase.getValue().getCollection("users"));
    }

    public void setFriendRequests() {
        this.friendRequests.setValue(mongoDatabase.getValue().getCollection("friendRequests"));
    }

    public void setUserCoordinates() {
        this.userCoordinates.setValue(mongoDatabase.getValue().getCollection("userCoordinates"));
    }

    public void setSendToHelper() {
        this.sendToHelper.setValue(mongoDatabase.getValue().getCollection("sendToHelper"));
    }

    public void setClosestUser(String username, Double lat, Double lon){
        this.closestUser = username;
        this.lat = lat;
        this.lon = lon;
    }

    public ArrayList<String> getReceivedFriendRequests() {
        return receivedFriendRequests;
    }

    public void setReceivedFriendRequests(ArrayList<String> receivedFriendRequests) {
        this.receivedFriendRequests = receivedFriendRequests;
    }

    public MutableLiveData<App> getApp() {
        return app;
    }

    public MutableLiveData<User> getMongoUser() {
        return mongoUser;
    }

    public MutableLiveData<MongoClient> getMongoClient() {
        return mongoClient;
    }

    public MutableLiveData<MongoDatabase> getMongoDatabase() {
        return mongoDatabase;
    }

    public MutableLiveData<MongoCollection<Document>> getUsers() {
        return users;
    }

    public MutableLiveData<MongoCollection<Document>> getFriendRequests() {
        return friendRequests;
    }

    public MutableLiveData<MongoCollection<Document>> getUserCoordinates() {
        return userCoordinates;
    }

    public MutableLiveData<MongoCollection<Document>> getSendToHelper() {
        return sendToHelper;
    }

    public String getClosestUser() {
        return closestUser;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getNeedingHelpUser() {
        return needingHelpUser;
    }

    public void setNeedingHelpUser(String needingHelpUser, Double lat, Double lon) {
        this.needingHelpUser = needingHelpUser;
        this.latHelp = lat;
        this.lonHelp = lon;
        this.isHelpAccepted = true;
    }

    public Double getLatHelp() {
        return latHelp;
    }

    public void setLatHelp(Double latHelp) {
        this.latHelp = latHelp;
    }

    public Double getLonHelp() {
        return lonHelp;
    }

    public void setLonHelp(Double lonHelp) {
        this.lonHelp = lonHelp;
    }

    public boolean isHelpAccepted() {
        return isHelpAccepted;
    }

    public void setHelpAccepted(boolean helpAccepted) {
        isHelpAccepted = helpAccepted;
    }
}
