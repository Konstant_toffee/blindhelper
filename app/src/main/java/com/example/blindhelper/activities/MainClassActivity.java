package com.example.blindhelper.activities;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.blindhelper.R;

import org.w3c.dom.Text;

import java.util.Locale;

import io.realm.Realm;

public class MainClassActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    TextToSpeech tts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tts = new TextToSpeech(this, this);
        Realm.init(this);
        setContentView(R.layout.mainclass_fragment);
    }

    public void speak(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS){
            tts.setLanguage(Locale.ENGLISH);
            tts.setPitch(1);

        } else {
            Log.e("TTS", "Failed");
        }
    }
}
