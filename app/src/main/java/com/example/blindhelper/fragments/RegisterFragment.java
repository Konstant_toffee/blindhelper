package com.example.blindhelper.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;
import com.example.blindhelper.mathfunctions.MathFunctions;
import com.example.blindhelper.model.LocalUser;
import com.example.blindhelper.model.UserCoordinates;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class RegisterFragment extends Fragment {
    EditText firstName, lastName, email, password, username, phone;
    Button createButton, switchLogin;
    CheckBox isBlind;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        checkPermission();

        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        model.setApp();

        username = view.findViewById(R.id.username);
        firstName = view.findViewById(R.id.firstName);
        lastName = view.findViewById(R.id.lastName);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        password = view.findViewById(R.id.password);
        createButton = view.findViewById(R.id.createButton);
        switchLogin = view.findViewById(R.id.switchLogin);
        isBlind = view.findViewById(R.id.isBlind);

        createButton.setOnClickListener(v -> {
            if(checkDataEntered()){
                isUsernameAvailable();

            }
        });

        switchLogin.setOnClickListener(z -> {
            navController.navigate(R.id.action_register_to_login);
        });
    }

    private void saveData() {
        Document localUser = new Document("username", username.getText().toString().trim())
                .append("firstName", firstName.getText().toString().trim())
                .append("lastName", lastName.getText().toString().trim())
                .append("isBlind", isBlind.isChecked())
                .append("email", email.getText().toString().trim())
                .append("password", password.getText().toString().trim())
                .append("phone", phone.getText().toString().trim())
                .append("closeFriend", "")
                .append("foundByFriendsOnly", false)
                .append("FriendList", new ArrayList<String>());

        model.setUser(new LocalUser(username.getText().toString().trim(),
                firstName.getText().toString().trim(),
                lastName.getText().toString().trim(),
                isBlind.isChecked(),
                email.getText().toString().trim(),
                password.getText().toString().trim(),
                phone.getText().toString().trim()));

        model.getUsers().getValue().insertOne(localUser).getAsync(task -> {
            if (task.isSuccess()) {
                Log.v("EXAMPLE", "successfully inserted a document with id: " + task.get());
            } else {
                Log.e("EXAMPLE", "failed to insert documents with: " + task.getError().getErrorMessage());
            }
        });

        Double lat = MathFunctions.giveCoordinate(true);
        Double lon = MathFunctions.giveCoordinate(false);

        model.setCoordinates(new UserCoordinates(username.getText().toString().trim(), lat, lon));

        Document userWithCoordinates = new Document("username", username.getText().toString().trim());
        userWithCoordinates.append("lat", lat);
        userWithCoordinates.append("lon", lon);

        model.getUserCoordinates().getValue().insertOne(userWithCoordinates).getAsync(task -> {
            if (task.isSuccess()) {
                Log.v("EXAMPLE", "successfully inserted a document with id: " + task.get());
            } else {
                Log.e("EXAMPLE", "failed to insert documents with: " + task.getError().getErrorMessage());
            }
        });
    }

    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    public void isUsernameAvailable(){
        Document queryFilter  = new Document("username", username.getText().toString().trim());
        model.getUsers().getValue().findOne(queryFilter).getAsync(task -> {
            if (task.isSuccess()) {
                Document result = task.get();
                if(result != null){
                    username.setError("The username is already taken!");
                } else{
                    saveData();
                    navController.navigate(R.id.action_register_to_mainPage);
                }
            } else{
                Log.e("EXAMPLE", "failed to find document with: ", task.getError());
            }
        });
    }

    protected boolean checkDataEntered() {
        boolean check = true;
        if (isEmpty(username)) {
            username.setError("Username is required!");
            check = false;
        }

        if (isEmpty(firstName)) {
            firstName.setError("First name is required!");
            check = false;
        }
        if (isEmpty(lastName)) {
            lastName.setError("Last name is required!");
            check = false;
        }
        if (!isEmail(email)) {
            email.setError("Enter valid email!");
            check = false;
        }
        if (isEmpty(email)) {
            email.setError("Email is required!");
            check = false;
        }
        if (isEmpty(phone)) {
            phone.setError("Enter valid phone number!");
            check = false;
        }
        if(isEmpty(password)){
            password.setError("Password is required!");
            check = false;
        }
        return check;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void checkPermission() {
        if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.CALL_PHONE,}, 1);
        }
        if(getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, 2);
        }
    }
}