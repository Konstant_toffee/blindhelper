package com.example.blindhelper.fragments;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.blindhelper.R;
import com.example.blindhelper.viewmodel.ViewModelApp;
import com.example.blindhelper.model.LocalUser;
import com.example.blindhelper.model.UserCoordinates;

import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class LoginFragment extends Fragment implements View.OnClickListener {
    EditText username, password;
    Button loginButton, switchRegister;
    NavController navController;
    ViewModelApp model;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        navController = Navigation.findNavController(view);
        model = new ViewModelProvider(getActivity()).get(ViewModelApp.class);

        username = view.findViewById(R.id.username);
        password = view.findViewById(R.id.password);
        loginButton = view.findViewById(R.id.loginButton);
        switchRegister = view.findViewById(R.id.switchRegister);

        loginButton.setOnClickListener(this);

        switchRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                if(checkDataEntered()){

                    Document queryCoordinates  = new Document("username", username.getText().toString().trim());
                    model.getUserCoordinates().getValue().findOne(queryCoordinates).getAsync(task -> {
                        if (task.isSuccess()) {
                            Document result = task.get();
                            if(result == null){
                                Log.v("Error", "No such user!");
                            } else {
                                model.setCoordinates(new UserCoordinates(result.getString("username"),
                                        result.getDouble("lat"),
                                        result.getDouble("lon")));
                            }
                        } else {
                            Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                        }
                    });

                    Document queryFilter  = new Document("username", username.getText().toString().trim()).append("password", password.getText().toString().trim());
                    model.getUsers().getValue().findOne(queryFilter).getAsync(task -> {
                        if (task.isSuccess()) {
                            Document result = task.get();
                            if(result == null){
                                username.setError("Check your username!");
                                password.setError("Check your password!");
                            } else {
                                Log.v("EXAMPLE", "successfully found a document: " + result);

                                model.setUser(new LocalUser(result.getString("username"),
                                        result.getString("firstName"),
                                        result.getString("lastName"),
                                        result.getBoolean("isBlind"),
                                        result.getString("email"),
                                        result.getString("password"),
                                        result.getString("phone"),
                                        result.getString("closeFriend"),
                                        result.getBoolean("foundByFriendsOnly"),
                                        (ArrayList<String>) result.getList("FriendList", String.class)));

                                navController.navigate(R.id.action_login_to_mainPage);
                            }
                        } else {
                            Log.e("EXAMPLE", "failed to find document with: ", task.getError());
                        }
                    });

                }
                break;
            case R.id.switchRegister:
                navController.navigate(R.id.action_login_to_register);
                break;
        }
    }

    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    protected boolean checkDataEntered() {
        if(isEmpty(password)){
            password.setError("Password is required!");
            return false;
        }
        return true;
    }
}