package com.example.blindhelper.model;

import java.util.ArrayList;

public class LocalUser {
    private String username;
    private String firstName;
    private String lastName;
    private boolean isBlind;
    private String email;
    private String password;
    private String phone;
    private String closeFriend;
    private boolean foundByFriendsOnly;
    private ArrayList<String> friendList;

    public LocalUser(String username, String firstName, String lastName, boolean isBlind, String email, String password, String phone) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isBlind = isBlind;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.closeFriend = "";
        this.foundByFriendsOnly = false;
        this.friendList = new ArrayList<>();
    }

    public LocalUser(String username, String firstName, String lastName, boolean isBlind, String email, String password, String phone, String closeFriend, boolean foundByFriendsOnly, ArrayList<String> friendList) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isBlind = isBlind;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.closeFriend = closeFriend;
        this.foundByFriendsOnly = foundByFriendsOnly;
        this.friendList = friendList;
    }

    public LocalUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isBlind() {
        return isBlind;
    }

    public void setBlind(boolean blind) {
        isBlind = blind;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCloseFriend() {
        return closeFriend;
    }

    public void setCloseFriend(String closeFriend) {
        this.closeFriend = closeFriend;
    }

    public ArrayList<String> getFriendList() {
        return friendList;
    }

    public void setFriendList(ArrayList<String> friendList) {
        this.friendList = friendList;
    }

    public boolean isFoundByFriendsOnly() {
        return foundByFriendsOnly;
    }

    public void setFoundByFriendsOnly(boolean foundByFriendsOnly) {
        this.foundByFriendsOnly = foundByFriendsOnly;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isBlind=" + isBlind +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", closeFriend='" + closeFriend + '\'' +
                ", friendList=" + friendList +
                '}';
    }
}
